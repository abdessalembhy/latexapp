// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
var app = require('electron').remote;
var dialog = app.dialog;
var fs = require('fs');
var x = function(err, contents) {

   var regex = /\$/gi, result, indices = [];
   
   while ( (result = regex.exec(contents)) ) {
      indices.push(result.index);
   }
   
   for (var i=0; i<=indices.length ;i++) {
       if (i % 2 == 0) {
       contents = contents.replace(/\$/, '[mathjaxinline]');
       }
       else {
       contents = contents.replace(/\$/, '[/mathjaxinline]');
       }
      };
      contents = contents.replace(/\\vskip\s2mm/g, ' ');
   contents = contents.replace(/\\vskip\s1mm/g, ' ');
   contents = contents.replace(/\\vskip\s1.5mm/g, ' ');
   contents = contents.replace(/\\scriptsize/g, ' ');
   contents = contents.replace(/\\scr/g, ' ');
   
   
   contents = contents.replace(/\\vect/g, '\\vec');
      contents = contents.replace(/\\cc/g, '\\checkmark');
      contents = contents.replace(/\\textbullet/g, '\\bullet');
      contents = contents.replace(/\\ds/g, '\\displaystyle');
      contents = contents.replace(/\\cRM\{1\}/g, 'I');
      contents = contents.replace(/\\cRM\{2\}/g, 'II');
      contents = contents.replace(/\\cRM\{3\}/g, 'III');
      console.log(indices)
      console.log(contents);
   
   }
    
document.getElementById('createButton').onclick=() => {
   dialog.showSaveDialog((fileName) => {
if(fileName === undefined) {
   alert("You didn't save a file ");
   return;
}
fs.writeFile('coursMathJaxLine.txt', contents, (err) => { 
   if (err) throw err; 
})

})
};
fs.readFile('./cours reflexion et guidage.tex', 'utf8', x);
  
